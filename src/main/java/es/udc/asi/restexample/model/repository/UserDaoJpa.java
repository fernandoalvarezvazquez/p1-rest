package es.udc.asi.restexample.model.repository;

import java.util.List;

import javax.persistence.TypedQuery;

import es.udc.asi.restexample.model.domain.Post;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;

import es.udc.asi.restexample.model.domain.User;
import es.udc.asi.restexample.model.repository.util.GenericDaoJpa;

@Repository
public class UserDaoJpa extends GenericDaoJpa implements UserDao {

  @Override
  public List<User> findAll() {
    return entityManager.createQuery("from User", User.class).getResultList();
  }

  @Override
  public User findById(Long id) {
    return entityManager.find(User.class, id);
  }

  @Override
  public User findByLogin(String login) {
    TypedQuery<User> query = entityManager.createQuery("from User u where u.login = :login", User.class)
        .setParameter("login", login);
    return DataAccessUtils.singleResult(query.getResultList());
  }

  @Override
  public List<Post> findAllPosts(Long id) {
    return entityManager.createQuery("from Post p where p.author.id = :id", Post.class).setParameter("id", id).getResultList();
  }

  @Override
  public void create(User user) {
    entityManager.persist(user);
  }

  @Override
  public void update(User user) {
    entityManager.merge(user);
  }
}
