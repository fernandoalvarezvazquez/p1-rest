package es.udc.asi.restexample.web;

import java.util.List;

import es.udc.asi.restexample.model.domain.Post;
import es.udc.asi.restexample.model.exception.NotFoundException;
import es.udc.asi.restexample.model.service.dto.PostDTO;
import es.udc.asi.restexample.web.exceptions.IdAndBodyNotMatchingOnUpdateException;
import es.udc.asi.restexample.web.exceptions.RequestBodyNotValidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import es.udc.asi.restexample.model.service.TagService;
import es.udc.asi.restexample.model.service.dto.TagDTO;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/tags")
public class TagResource {

  @Autowired
  private TagService tagService;

  @GetMapping
  public List<TagDTO> findAll() {
    return tagService.findAll();
  }

  @GetMapping("/{id}")
  public TagDTO findOne(@PathVariable Long id) throws NotFoundException {
    return tagService.findById(id);
  }

  @PostMapping
  public TagDTO create(@RequestBody @Valid TagDTO tag, Errors errors) throws RequestBodyNotValidException {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }
    return tagService.create(tag);
  }


}
